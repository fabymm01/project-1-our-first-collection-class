package symbol;
import static org.junit.Assert.*;

import org.junit.Test;

import symbol.Symbol;
import symbol.SymbolDictionary;
import symbol.SymbolIO;

/**
 * 
 */

/**
 * @author mmfab
 *
 */
public class projectOneTests {

	@Test
	public void symbolsreadertest() {
		
		//Checks to see if the class can read sample.java

		SymbolIO si = new SymbolIO();
		Symbol s = new Symbol();//making symbol test case
		s.setStrToken("java");
		si.reader("sample.java");//reads in file
		SymbolDictionary sd = si.getSd();
		assertTrue(sd.isPresent(s));//Tests that the file was read in correctly

	}
	
	@Test
	public void removepart1() {
		
		
		//Checks if the remove works 
		//Remove is a split into  two part test.
		//The first part is showing that symbol is there before remove.
		//The second part test that the symbols is not there after the removing process is done.
		SymbolIO si = new SymbolIO();
		Symbol s = new Symbol();
		s.setStrToken("import");
		si.reader("sample.java");
		SymbolDictionary sd = si.getSd();
		assertTrue(sd.isPresent(s));
		
		
	}
	
	
	@Test
	public void removepart2() {
		SymbolIO si = new SymbolIO();
		Symbol s = new Symbol();
		s.setStrToken("import");
		si.reader("sample.java");
		si.removeSymbols("removeSymbol.dat");
		SymbolDictionary sd = si.getSd();
		assertFalse(sd.isPresent(s));
		
		
	}
	
	
	
	@Test
	public void removeBreak() {
		
		//Check to see if remove does not break the dictionary.
		SymbolIO si = new SymbolIO();
		Symbol s = new Symbol();
		s.setStrToken("import");
		si.reader("sample.java");
		si.removeSymbols("removeSymbol.dat");
		SymbolDictionary sd = si.getSd();
		
		
		s.setStrToken("import");
		sd.remove(s);
		assertEquals("sdjkdj",sd.getManyitems(),38);
		/*
		 * To Test this I am showing that number symbol in the dictionary does
		 * not increase from the remove function 
		 */
		
		
	}
	
	
	
	
	@Test
	public void findindexnewSymbol() {
		//find the index of the symbol to be found.
		
		
		SymbolIO si = new SymbolIO();
		Symbol s = new Symbol();
		s.setStrToken("import");
		si.reader("sample.java");
		si.removeSymbols("removeSymbol.dat");
		SymbolDictionary sd = si.getSd();
		/*
		 * shows that symbol imports is found at index 1.
		 * That is has been added back into the dictionary.
		 */
		assertEquals("sdjkdj",sd.findindex(s),0);
		
		
	}
	
	
	
	@Test
	public void findindexoldSymbol() {
		
		//That find index on a symbol in the dictionary can be found.
		SymbolIO si = new SymbolIO();
		Symbol s = new Symbol();
		s.setStrToken("import");
		si.reader("sample.java");
		
		SymbolDictionary sd = si.getSd();
		//The index of import is 0.
		assertEquals("sdjkdj",sd.findindex(s),0);
		
		
	}

	
	
	
	

}
