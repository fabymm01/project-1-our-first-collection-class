package symbol;

public class Symbol implements Cloneable{
	
	
	
	public Symbol() {
		
		//Sets up the strToken String 
		
		
		strToken= "";
	}
	
	
	
	private String strToken;
	//Can get the string in object Symbol.

	public String getStrToken() {
		
		return strToken;
	}
//Can set the String in object Symbol
	public void setStrToken(String strToken) {
		this.strToken = strToken;
	}
	
	
	/*This method checks if the symbols are the same.
	 * 
	 */
public boolean equals (Symbol sym) {
	
	if (sym.getStrToken().equals(getStrToken())) {
		
		
		
		return true;
	}
	
	
	
	
	return false;
	
	
	
	
	
}
/*This methods clones the object with super.clone.
 * 
 * (non-Javadoc)
 * 
 * @see java.lang.Object#clone()
 */

public Symbol clone() {
	try {
		Symbol copy = (Symbol) super.clone();
		
		copy.setStrToken(getStrToken());
		
		
		return copy;
	
	} catch (CloneNotSupportedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return null;
}
	

}
