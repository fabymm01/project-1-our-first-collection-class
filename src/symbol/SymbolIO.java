package symbol;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class SymbolIO {

	private SymbolDictionary sd;
	
	
	public SymbolDictionary getSd() {
		return sd;
	}

	public void setSd(SymbolDictionary sd) {
		this.sd = sd;
	}
/*this method take the file input by the user and removes
 * the symbols from symbolDictionary.
 */
	public void removeSymbols(String s) {
		
		String removes[]= new String [90];

		File f = new File(s);

		try {
			Scanner sc = new Scanner(f);
		
			
			while (sc.hasNextLine()) {
			
				
				removes[0]=sc.nextLine();
				
			Symbol symbol = new Symbol();
			
			symbol.setStrToken(removes[0]);
			
			sd.remove(symbol);
			
			}

			sc.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public SymbolIO() {

		sd = new SymbolDictionary();

	}
/*This method write the dictionary to a file
 * with the indexes next to the symbol.
 */
	public void writer() {

		try {
			PrintWriter pw = new PrintWriter("Simple.out");

			pw.write(sd.getManyitems()+"\n");

			Symbol symbol[] = sd.getS();

			for (int i = 0; i < symbol.length; i++) {

				if (symbol[i].getStrToken().equals("")) {

				} else {

					pw.write(symbol[i].getStrToken() + " " + sd.findindex(symbol[i])+"\n");

				}

			}

			pw.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
/*This method reads in the file that starts the symboldictionary 
 * and makes symbols that can only be A-z.
 */
	public void reader(String s) {
		File f = new File(s);

		try {
			Scanner sc = new Scanner(f);

			while (sc.hasNextLine()) {

				String tokens[] = sc.nextLine().split("[^A-z]");

				for (int i = 0; i < tokens.length; i++) {
					Symbol createSymbol = new Symbol();

					createSymbol.setStrToken(tokens[i]);

					sd.findindex(createSymbol);

				}

			}
			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
