package symbol;

import java.util.Arrays;

public class SymbolDictionary {

	private Symbol s[];

	private int manyitems;

	public SymbolDictionary() {
		
		//sets up the array of the object of symbols.

		s = new Symbol[200];

		manyitems = 0;

		for (int i = 0; i < 200; i++) {

			s[i] = new Symbol();

			s[i].setStrToken("");

		}

	}

	public int findindex(Symbol sym) {
		
		//finds the index of the symbol in the dictionary.
		for (int i = 0; i < 200; i++) {
			if (sym.equals(s[i])) {

				return i;

			}
		}
//if the symbol is not in the dictionary it is added.
		for (int i = 0; i < 200; i++) {

			if (s[i].getStrToken().equals("")) {

				s[i] = sym;

				manyitems++;

				return i;
			}

		}
		//if the dictionary is full it returns a negative if the dictionary is full

		return -1;
	}
//Gets the array of symbols
	public Symbol[] getS() {
		return s;
	}
//sets the array of symbols
	public void setS(Symbol[] s) {
		this.s = s;
	}
//gets the count of Symbols
	public int getManyitems() {
		return manyitems;
	}

	public void setManyitems(int manyitems) {
		this.manyitems = manyitems;
	}
/*Removes symbol in the dictionary if it is there.
 * 
 */
	public void remove(Symbol sym) {

		for (int i = 0; i < 200; i++) {
			if (sym.equals(s[i])) {

				s[i].setStrToken("");
				manyitems--;
				break;

			}
		}

	}
	/*Checks and sees if the symbol is in the dictionary.
	 * 
	 */

	public boolean isPresent(Symbol sym) {

		for (int i = 0; i < 200; i++) {
			if (sym.equals(s[i])) {

				return true;

			}
		}

		return false;

	}

}
