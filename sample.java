import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class sample extends JApplet implements ActionListener {
  JButton redButton, greenButton, blueButton;
  JLabel currentLabel;
  JPanel panel;

  public void init()
  {
    Container c = getContentPane();
    c.setLayout(new GridLayout(2,1));
    c.setBackground(Color.white);

    panel = new JPanel();
    redButton = new JButton("Red");
    greenButton = new JButton("Green");
    blueButton = new JButton("Blue");
    currentLabel = new JLabel("Color Example", SwingConstants.CENTER);

    redButton.setBackground(Color.red);
    greenButton.setBackground(Color.green);
    blueButton.setBackground(Color.blue);
    currentLabel.setForeground(Color.red);

    panel.add(redButton);
    panel.add(greenButton);
    panel.add(blueButton);
    c.add(panel);
    c.add(currentLabel);
    redButton.addActionListener(this);
    greenButton.addActionListener(this);
    blueButton.addActionListener(this);
  }

  public void actionPerformed(ActionEvent e)
  {
    if (e.getSource() == redButton) 
				currentLabel.setForeground(Color.red);
    else if (e.getSource() == greenButton) 
				currentLabel.setForeground(Color.green);
    else if (e.getSource() == blueButton) 
				currentLabel.setForeground(Color.blue);
  }
}

